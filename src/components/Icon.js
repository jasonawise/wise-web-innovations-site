import React from "react";

class Icon extends React.Component {
  render() {
    return (
      <React.Fragment>
        <i className={`${this.props.icon} ${this.props.size}`} />
      </React.Fragment>
    );
  }
}
export default Icon;
