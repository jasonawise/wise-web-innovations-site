import React from "react";
import "../css/Hero.css";

class Hero extends React.Component {
  render() {
    return (
      <section className="hero">
        <img className="hero-image mb-5" src={this.props.imgSource} alt="" />
        <div className="container-fluid hero-text">
          <h1>{this.props.tagline}</h1>
          <a href="contact">
            <button type="button" className="btn btn-outline-dark btn-lg mr-1">
              {this.props.callToAction}
            </button>
          </a>
          <a href="#mission">
            <button type="button" className="btn btn-outline-dark btn-lg ml-1">
              {this.props.mission}
            </button>
          </a>
        </div>
      </section>
    );
  }
}

export default Hero;
