import React from "react";
import NavBar from "./NavBar";
import Footer from "./Footer";
import Router from "./Router";

class App extends React.Component {
  render() {
    return (
      <React.Fragment>
        <NavBar
          brand="Wise Web Innovations"
          link1="Home"
          link2="Services"
          link3="About"
          link4="Contact"
          imgSrc="/images/Logo.svg"
        />
        <Router />
        <Footer />
      </React.Fragment>
    );
  }
}

export default App;
