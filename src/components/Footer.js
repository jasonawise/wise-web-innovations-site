import React from "react";
import "../css/Footer.css";
import Icon from "./Icon";

class Footer extends React.Component {
  render() {
    // const data = jsonResponse.MissionData;
    return (
      <React.Fragment>
        <footer className="footer p-3">
          <div className="container">
            <Icon icon="fab fa-facebook-f" />
          </div>
        </footer>
      </React.Fragment>
    );
  }
}

export default Footer;
