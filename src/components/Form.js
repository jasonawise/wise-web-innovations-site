import React from "react";
import "../css/Form.css";

class Form extends React.Component {
  render() {
    return (
      <React.Fragment>
        <div>
          <form
            action="https://formspree.io/wisewebinnovations@gmail.com"
            method="POST"
          >
            <div className="form-row form-group">
              <div className="col">
                <input
                  type="text"
                  className="form-control"
                  name="name"
                  id="name"
                  placeholder="Name"
                />
              </div>
              <div className="col">
                <input
                  type="text"
                  className="form-control"
                  name="company-name"
                  id="company-name"
                  placeholder="Company Name"
                />
              </div>
            </div>
            <div className="form-row form-group">
              <div className="col">
                <input
                  type="email"
                  className="form-control"
                  name="email"
                  id="email"
                  placeholder="Email Address"
                />
              </div>
            </div>
            <div className="col-auto">
              <button type="submit" className="btn btn-primary mb-2 btn-gold">
                Submit
              </button>
            </div>
          </form>
        </div>
      </React.Fragment>
    );
  }
}

export default Form;
