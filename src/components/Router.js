import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import NotFound from "./NotFound";
import Services from "./Services";
import Main from "./Main";
import About from "./About";
import Contact from "./Contact";

const Router = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path="/" component={Main} />
      <Route path="/services/" component={Services} />
      <Route path="/about/" component={About} />
      <Route path="/contact/" component={Contact} />
      <Route component={NotFound} />
    </Switch>
  </BrowserRouter>
);

export default Router;
