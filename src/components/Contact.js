import React from "react";
import Form from "./Form";
import { jsonResponse } from "../data";

class Contact extends React.Component {
  render() {
    const data = jsonResponse.ContactData;
    return (
      <React.Fragment>
        <div className="container contact-page">
          <h2>{data.pageTitle}</h2>
          <p>{data.content}</p>
          <Form />
        </div>
      </React.Fragment>
    );
  }
}

export default Contact;
