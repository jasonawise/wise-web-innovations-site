import React from "react";
import { jsonResponse } from "../data";
import Icon from "./Icon";

class Mission extends React.Component {
  render() {
    const data = jsonResponse.MissionData;
    return (
      <React.Fragment>
        <section className="mission">
          <div id="mission" className="container mt-5 mb-5 text-center">
            <h2>{data.title}</h2>
            <div className="row">
              <div className="col">
                <h3 className="mb-3">{data.simple.title}</h3>
                <Icon icon="far fa-edit" size="fa-5x" />
                <p className="text-left mt-3">{data.simple.content}</p>
              </div>
              <div className="col">
                <h3 className="mb-3">{data.custom.title}</h3>
                <Icon icon="fas fa-code" size="fa-5x" />
                <p className="text-left mt-3">{data.custom.content}</p>
              </div>
            </div>
            <div className="row">
              <div className="col">
                <a href="services">
                  <button
                    type="button"
                    className="btn btn-outline-dark btn-lg mr-3"
                  >
                    {data.cta}
                  </button>
                </a>

                <a href="contact">
                  <button
                    type="button"
                    className="btn btn-outline-dark btn-lg m1-3"
                  >
                    {data.contact}
                  </button>
                </a>
              </div>
            </div>
          </div>
        </section>
      </React.Fragment>
    );
  }
}

export default Mission;
