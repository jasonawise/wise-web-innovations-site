import React from "react";
import Hero from "./Hero";
import Mission from "./Mission";

import { jsonResponse } from "../data";

class Main extends React.Component {
  render() {
    const data = jsonResponse.HeroData;
    return (
      <React.Fragment>
        <Hero
          tagline={data.tagline}
          callToAction={data.cta}
          mission={data.mission}
          imgSource={data.imgSource}
        />
        <Mission />
      </React.Fragment>
    );
  }
}

export default Main;
