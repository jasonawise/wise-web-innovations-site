import React from "react";
import "../css/NavBar.css";

class NavBar extends React.Component {
  render() {
    return (
      <nav className="navbar navbar-expand-sm navbar-light bg-light">
        <a className="navbar-brand" href="#">
          <img src={this.props.imgSrc} width="300" alt="" />
        </a>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarNav"
          aria-controls="navbarNav"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon" />
        </button>
        <div className="collapse navbar-collapse" id="navbarNav">
          <ul className="navbar-nav ml-auto">
            <li className="nav-item">
              <a className="nav-link" href="/">
                {this.props.link1}
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href={this.props.link2}>
                {this.props.link2}
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href={this.props.link3}>
                {this.props.link3}
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href={this.props.link4}>
                {this.props.link4}
              </a>
            </li>
          </ul>
        </div>
      </nav>
    );
  }
}

export default NavBar;
