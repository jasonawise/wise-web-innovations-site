import React from "react";
import Basic from "./Basic";
import { jsonResponse } from "../data";
import Advanced from "./Advanced";
import Custom from "./Custom";
import "../css/Packages.css";

class Packages extends React.Component {
  render() {
    const data = jsonResponse.ServiceData.packages;
    return (
      <React.Fragment>
        <section>
          <h3 className="mb-5">{this.props.title}</h3>
          <div className="row">
            <Basic title={data.basic.title} content={data.basic.content} />
            <Advanced
              title={data.advanced.title}
              content={data.advanced.contect}
            />
            <Custom title={data.custom.title} content={data.custom.content} />
          </div>

          <a href="contact">
            <button type="button" className="btn btn-outline-dark btn-lg mr-1">
              {data.cta}
            </button>
          </a>
        </section>
      </React.Fragment>
    );
  }
}

export default Packages;
