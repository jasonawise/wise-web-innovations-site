import React from "react";
import Packages from "./Packages";
import { jsonResponse } from "../data";

class Services extends React.Component {
  render() {
    const data = jsonResponse.ServiceData;
    return (
      <React.Fragment>
        <div className="container-fluid text-center">
          <h2 className="mt-3 mb-3">{data.title}</h2>
          <img className="img-fluid rounded mb-5" src={data.image} alt="" />
        </div>
        <div className="container-fluid packages text-center mb-5">
          <Packages title={data.packages.title} />
        </div>
      </React.Fragment>
    );
  }
}

export default Services;
