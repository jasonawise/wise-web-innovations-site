import React from "react";

class Advanced extends React.Component {
  render() {
    return (
      <React.Fragment>
        <div className="col-md">
          <h4 className="mb-3">
            <u>{this.props.title}</u>
          </h4>
          <p>{this.props.content}</p>
        </div>
      </React.Fragment>
    );
  }
}

export default Advanced;
