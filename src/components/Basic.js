import React from "react";
import Icon from "./Icon";

class Basic extends React.Component {
  render() {
    return (
      <React.Fragment>
        <div className="col-md">
          <h4 className="mb-3">
            <u>{this.props.title}</u>
          </h4>
          <p>{this.props.content}</p>
        </div>
      </React.Fragment>
    );
  }
}

export default Basic;
