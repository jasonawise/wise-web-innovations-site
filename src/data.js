const jsonResponse = {
  HeroData: {
    tagline: "We build the web so you don't have to.",
    cta: "Let's Innovate",
    mission: "Our Mission",
    imgSource: "/images/desk.jpeg"
  },
  ContactData: {
    pageTitle: "Contact",
    content:
      "Have questions? Want a free consultation? Need more information? Fill out this simple form and we will contact you shortly!",
    imgSource: "/images/red-phone.jpg"
  },
  MissionData: {
    title: "Our Mission",
    simple: {
      title: "Simple",
      content:
        "We're not here to confuse you with terms and languages." +
        " Your least worry should be what programming language or CMS your using." +
        " We get right to it and build a site that works for you."
    },
    cta: "Services",
    contact: "Let's Get Started",
    custom: {
      title: "Custom",
      content:
        "We get to know you and your needs." +
        " From there we work to taylor your needs to a site that is perfect for you." +
        " We work to make sure everything is there that suits you with no extra features."
    }
  },
  ServiceData: {
    title: "Services",
    image: "/images/services.jpeg",
    packages: {
      title: "Site Types",
      cta: "Get a Quote",
      basic: {
        title: "Basic",
        content:
          "This is a basic landing page or brochure-style site this is easy to setup online." +
          " This is an ideal solution if you do not want to update your site."
      },
      advanced: {
        title: "Advanced",
        contect:
          "This site gives your the ability to update your site yourself." +
          " If you want the ability to update information, like paragraphs or images, this is the solution for you."
      },
      custom: {
        title: "Custom",
        content:
          "Need something a lot more custom? This allows you full control of your site." +
          " Payment integration, advanded user memership options, online store.  This is the solution for you."
      }
    }
  }
};

export { jsonResponse };
